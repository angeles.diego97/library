from rest_framework import filters, generics
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response

from member.permissions import IsLibrarian

from .models import Book, BookItem, BookLend
from .serializers import (
    BookItemReserveSerializer,
    BookItemSerializer,
    BookLendSerializer,
    BookReturnSerializer,
    BookSerializer,
)


class BookItemListAPIView(generics.ListCreateAPIView):
    permission_classes = [IsLibrarian | IsAdminUser]
    queryset = BookItem.objects.all()
    serializer_class = BookItemSerializer


class BookItemDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [IsLibrarian | IsAdminUser]
    queryset = BookItem.objects.all()
    serializer_class = BookItemSerializer


class BookSearchAPIView(generics.ListAPIView):
    queryset = Book.objects.all()
    serializer_class = BookSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = [
        "title",
        "author",
        "isbn",
        "publication_date",
        "subject",
    ]


class BookCheckOutAPIView(generics.CreateAPIView):
    queryset = BookLend.objects.all()
    serializer_class = BookLendSerializer

    def create(self, request, *args, **kwargs):
        user = request.user
        if user.total_books_checked_out < 5:
            super().create(request)
            user.total_books_checked_out += 1
            user.save()

            book_item = BookItem.objects.filter(id=request.POST["book_item"])
            for b_item in book_item:
                if b_item.loaned_to is not None:
                    return Response({"failed": "This book is already checked out"})
                elif b_item.reserved_by is not user or b_item.reserved_by is not None:
                    return Response(
                        {"failed": "This books has already been checked-out"}
                    )
            book_item.update(loaned_to=user)

            return Response({"success"})
        else:
            return Response({"You can't check-out more than 5 books"})


class ReturnBookAPIView(generics.CreateAPIView):
    queryset = BookLend.objects.all()
    serializer_class = BookReturnSerializer

    def create(self, request, *args, **kwargs):
        book_lend_id = self.kwargs["pk"]
        user = request.user
        user.total_books_checked_out -= 1
        user.save()

        book_item = BookItem.objects.filter(id=request.POST["book_item"])
        book_item.update(loaned_to=None)

        book_lend = BookLend.objects.filter(
            member=user, book_item=book_lend_id, status="on"
        )
        book_lend.update(status="cd")

        return Response({"success"})


class ReserveBookAPIView(generics.CreateAPIView):
    queryset = BookItem.objects.all()
    serializer_class = BookItemReserveSerializer

    def create(self, request, *args, **kwargs):
        user = request.user
        can_reserve = True
        book_item = BookItem.objects.filter(book=request.POST["book"])
        for b_item in book_item:
            if b_item.reserved_by == user:
                return Response({"failed: You already reserved this book"})

        for b_item in book_item:
            if b_item.reserved_by is None:
                b_item.reserved_by = user
                can_reserve = False
                b_item.save()
                break
        if can_reserve:
            return Response({"failed": "No book was found available to reserve"})

        return Response({"success"})


class BookItemSearchAPIView(generics.ListAPIView):
    queryset = BookItem.objects.all()
    serializer_class = BookItemSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = [
        "book__title",
        "loaned_to__email",
        "reserved_by__email",
        "status",
        "unique_id",
    ]
