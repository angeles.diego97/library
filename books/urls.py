# bookItems/urls.py
from django.urls import path

from .views import (
    BookCheckOutAPIView,
    BookItemDetailAPIView,
    BookItemListAPIView,
    BookItemSearchAPIView,
    BookSearchAPIView,
    ReserveBookAPIView,
    ReturnBookAPIView,
)

urlpatterns = [
    path("books/<int:pk>/", BookItemDetailAPIView.as_view(), name="book_item_detail"),
    path("books", BookItemListAPIView.as_view(), name="book_item_list"),
    path("books/search", BookSearchAPIView.as_view(), name="book_search_list"),
    path("books/check-out", BookCheckOutAPIView.as_view(), name="book_check_out"),
    path("books/reserve/", ReserveBookAPIView.as_view(), name="book_reserve"),
    path("books/return/<int:pk>", ReturnBookAPIView.as_view(), name="book_return"),
    path(
        "books/search-item",
        BookItemSearchAPIView.as_view(),
        name="book_item_search_list",
    ),
]
