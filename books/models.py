import uuid

from django.conf import settings
from django.db import models


class Book(models.Model):
    title = models.CharField(max_length=50)
    author = models.CharField(max_length=50)
    isbn = models.CharField(max_length=13)
    publication_date = models.DateField()
    subject = models.CharField(max_length=50)

    def __str__(self):
        return self.title


class BookItem(models.Model):
    status_choices = [
        ("ae", "available"),
        ("ld", "loaned"),
        ("lt", "lost"),
    ]
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    loaned_to = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        related_name="related_loaned_to",
    )
    reserved_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        related_name="related_reserved_by",
    )
    rack_identifier = models.CharField(max_length=10)
    status = models.CharField(choices=status_choices, max_length=2)
    unique_id = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "Title: %s at Rack: %s" % (self.book.title, self.rack_identifier)


class BookLend(models.Model):
    status_choices = [
        ("on", "open"),
        ("cd", "closed"),
    ]
    creation_date = models.DateField(auto_now_add=True)
    return_date = models.DateField()
    member = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    book_item = models.ForeignKey(BookItem, on_delete=models.CASCADE)
    status = models.CharField(choices=status_choices, max_length=2)

    def __str__(self):
        return "User: %s lent the Book: %s from Rack: %s" % (
            self.member.username,
            self.book_item.book.title,
            self.book_item.rack_identifier,
        )
