from rest_framework import serializers

from .models import Book, BookItem, BookLend


class BookItemSerializer(serializers.ModelSerializer):
    created_at = serializers.ReadOnlyField()
    updated_at = serializers.ReadOnlyField()
    unique_id = serializers.ReadOnlyField()

    class Meta:
        fields = (
            "book",
            "loaned_to",
            "reserved_by",
            "rack_identifier",
            "status",
            "unique_id",
            "created_at",
            "updated_at",
        )
        model = BookItem
        depth = 1

    def to_representation(self, instance):
        response = super().to_representation(instance)

        for book_item in response.items():
            if book_item[0] == "loaned_to" or book_item[0] == "reserved_by":
                user = book_item[1]
                if user:
                    user.pop("password", None)
                    user.pop("last_login", None)
                    user.pop("is_superuser", None)
                    user.pop("is_staff", None)
                    user.pop("is_active", None)
                    user.pop("date_joined", None)
                    user.pop("groups", None)
                    user.pop("user_permissions", None)
        return response


class BookSerializer(serializers.ModelSerializer):
    class Meta:
        fields = (
            "title",
            "author",
            "isbn",
            "publication_date",
            "subject",
        )
        model = Book


class BookLendSerializer(serializers.ModelSerializer):
    creation_date = serializers.ReadOnlyField()
    status = serializers.ReadOnlyField()

    class Meta:
        fields = (
            "creation_date",
            "return_date",
            "member",
            "book_item",
            "status",
        )
        model = BookLend


class BookReturnSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ("book_item",)
        model = BookLend


class BookItemReserveSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ("book",)
        model = BookItem
