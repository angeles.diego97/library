# Generated by Django 4.0.6 on 2022-07-30 21:05

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("books", "0005_alter_booklend_member"),
    ]

    operations = [
        migrations.AlterField(
            model_name="booklend",
            name="book_item",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE, to="books.bookitem"
            ),
        ),
    ]
