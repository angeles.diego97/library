# Generated by Django 4.0.6 on 2022-07-28 22:47

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("books", "0003_alter_booklend_book"),
    ]

    operations = [
        migrations.RenameField(
            model_name="booklend",
            old_name="book",
            new_name="book_item",
        ),
    ]
