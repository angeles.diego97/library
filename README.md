# Library

Library Management System API

## Description

- [Base URL](https://academlo-library-api.herokuapp.com/api/v1)

## Resources

### Schema

- [Schema.yml](https://academlo-library-api.herokuapp.com/api/schema/)

### OpenAPI

- [OpenAPI docs](https://academlo-library-api.herokuapp.com/api/schema/redoc/)

### Swagger

- [Swagger Docs](https://academlo-library-api.herokuapp.com/api/schema/swagger-ui/)

## Author

[@Diego-Angeles-Ake](https://github.com/Diego-Angeles-Ake)

## Acknowledgments

- [Nicolás Rondón](https://github.com/NicolasRondonAcademlo)
