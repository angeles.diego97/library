from django.contrib.auth.models import AbstractUser
from django.db import models


class CustomUser(AbstractUser):
    city = models.CharField(null=True, blank=True, max_length=35)
    street_address = models.CharField(null=True, blank=True, max_length=95)
    phone = models.CharField(null=True, blank=True, max_length=12)
    total_books_checked_out = models.IntegerField(
        null=False, default=0)
    is_librarian = models.BooleanField(null=False, default=False)
