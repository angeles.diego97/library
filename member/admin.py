from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .forms import CustomUserChangeForm, CustomUserCreationForm
from .models import CustomUser


class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = CustomUser
    list_display = [
        "email",
        "username",
        "is_librarian",
        "is_staff",
    ]
    fieldsets = UserAdmin.fieldsets + \
        ((None, {"fields": ("city",
                            "street_address",
                            "phone",
                            "total_books_checked_out",
                            "is_librarian",)}),)
    add_fieldsets = UserAdmin.add_fieldsets + \
        ((None, {"fields": ("city",
                            "street_address",
                            "phone",
                            "total_books_checked_out",
                            "is_librarian",)}),)


admin.site.register(CustomUser, CustomUserAdmin)
