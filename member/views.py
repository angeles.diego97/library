from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response

from member.permissions import IsLibrarian, IsLibrarianAccOwner

from .serializers import UserSerializer


class UserViewSet(viewsets.ModelViewSet):
    def list(self, request):
        queryset = get_user_model().objects.all()
        serializer = UserSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = get_user_model().objects.all()
        user = get_object_or_404(queryset, pk=pk)
        serializer = UserSerializer(user)
        return Response(serializer.data)

    def get_permissions(self):

        if self.action == "list":
            permission_classes = [IsLibrarian]
        elif self.action == "retrieve":
            permission_classes = [IsLibrarianAccOwner]
        else:
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]
